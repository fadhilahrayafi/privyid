import React from 'react'
import './style.scss'
import {Navbar, Nav, Spinner} from 'react-bootstrap'
import {useHistory} from 'react-router-dom'
// import {revokeToken} from '../../Services'

export const HeaderComponent = ({name, selected}) => {
  const {push} = useHistory()

  const logout = () => {
    localStorage.clear()
    push('/login')
    // ERROR connecting to redis, tidak bisa dipakai
    // let payload = {
    //   access_token : JSON.parse(localStorage.getItem('token')).access_token,
    //   confirm : 1
    // }
    // revokeToken(payload, (err, res) => {
    //   if(err) {
    //     console.log(err.response)
    //   } else {
    //     localStorage.clear()
    //     push('/login')
    //   }
    // })
  }

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand >Hello {name ? name : ''}</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link onClick={() => push('/profile')} active={selected === "profile" ? true : false} >Profile</Nav.Link>
          <Nav.Link onClick={() => push('/message')} active={selected === "msg" ? true : false}>Message</Nav.Link>
        </Nav>
        <Nav>
          <Nav.Link onClick={() => logout()}>Logout</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

export const LoadingComponent = () => {
  return (
    <div style={{width:'100%', height:'100%', display:'flex', justifyContent:'center', alignItems:'center'}}>
      <Spinner animation="border" role="status"/> 
    </div>

  )
}