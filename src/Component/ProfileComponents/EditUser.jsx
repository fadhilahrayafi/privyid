import React, { Fragment, useState } from 'react'
import './style.scss'
import {Modal, Button, Form, Spinner} from 'react-bootstrap'
import {editProfile} from '../../Services'

export const EditUserComponent = ({show, handleClose, userData, reFetch}) => {
  const [name, setName] = useState(userData.name ? userData.name : ''),
        [gender, setGender] = useState(userData.gender === 1 ? 1 : 0),
        [birthday, setBirthday] = useState(userData.birthday ? userData.birthday : ''),
        [hometown, setHometown] = useState(userData.hometown ? userData.hometown : ''),
        [loading, setLoading] = useState(false),
        [errMsg, setErrMsg] = useState(''),
        [bio, setBio] = useState(userData.bio ? userData.bio : '');

  const editProfiles = () => {
    let payload = {
      name,
      gender: Number(gender),
      birthday,
      hometown,
      bio
    }
    setErrMsg('')
    setLoading(true)
    let token = JSON.parse(localStorage.getItem('token')).access_token
    if(name && gender && birthday && hometown && bio) {
      editProfile(token, payload, (err, res) => {
        if(err) {
          console.log(err)
          setErrMsg(err.response.data.error.errors[0])
          setLoading(false)
        } else {
          console.log(res)
          setLoading(false)
          reFetch()
          handleClose()
        }
      })
    }
  }

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Profile</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" placeholder="Enter name" value={name} onChange={({target: {value}}) => setName(value)}/>
          </Form.Group>
          <Form.Group controlId="formGridState">
            <Form.Label>Gender</Form.Label>
            <Form.Control as="select" defaultValue="Choose..." value={gender} onChange={({target: {value}}) => setGender(value)}>
              <option value={1}>Female</option>
              <option value={0}>Male</option>
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Birthday</Form.Label>
            <Form.Control type="date" placeholder="Enter birthday" value={birthday} onChange={({target: {value}}) => setBirthday(value)}/>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Hometown</Form.Label>
            <Form.Control type="text" placeholder="Enter hometown" value={hometown} onChange={({target: {value}}) => setHometown(value)}/>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Bio</Form.Label>
            <Form.Control type="text" placeholder="Enter bio" value={bio} onChange={({target: {value}}) => setBio(value)}/>
          </Form.Group>
          {loading ? 
            <Spinner animation="border" role="status"/> :
            <Fragment>
              <Button variant="primary" onClick={() => editProfiles()}> Edit</Button><br/>
            </Fragment>
          }
          {errMsg ? <span style={{color:'red'}}>{errMsg}</span> : null}
        </Form>
      </Modal.Body>
    </Modal>
  )
}