import React, { useState, Fragment } from 'react'
import './style.scss'
import { Modal, Button, Form, Spinner } from 'react-bootstrap'
import { UploadCover, setDefaultPict, UploadProfilePict } from '../../Services'

export const UploadCoverComp = ({ show, handleClose, userData, reFetch }) => {
  const [pict, setImage] = useState({}),
    [loading, setLoading] = useState(false),
    [errMsg, setErrMsg] = useState('');

  const setCover = () => {
    setErrMsg('')
    let token = JSON.parse(localStorage.getItem('token')).access_token
    setLoading(true)
    UploadCover(token, pict, (err, res) => {
      if (err) {
        console.log(err)
        setErrMsg(err.response.data.error.errors[0])
        setLoading(false)
      } else {
        console.log(res)
        setLoading(false)
        reFetch()
        handleClose()
      }
    })
  }

  function handelChangeImage(event) {
    let image = event.target.files[0];
    let form = new FormData();
    form.append('image', image);
    setImage(form)
  }

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Upload New Image</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label >Select File</Form.Label>
            <Form.File onChange={(e) => handelChangeImage(e)} />
          </Form.Group>
          {loading ?
            <Spinner animation="border" role="status" /> :
            <Fragment>
              <Button variant="primary" onClick={() => setCover()}>Edit Cover</Button><br />
            </Fragment>
          }
          {errMsg ? <span style={{ color: 'red' }}>{errMsg}</span> : null}
        </Form>
      </Modal.Body>
    </Modal>
  )
}

export const UploadProfilePictComp = ({ show, handleClose, userData, reFetch }) => {
  const [pict, setImage] = useState({}),
        [loading, setLoading] = useState(false),
        [errMsg, setErrMsg] = useState('');

  const setPicture = () => {
    setErrMsg('')
    let token = JSON.parse(localStorage.getItem('token')).access_token
    setLoading(true)
    UploadProfilePict(token, pict, (err, res) => {
      if (err) {
        console.log(err)
        setErrMsg('Failed to upload new picture')
        setLoading(false)
      } else {
        console.log(res)
        setLoading(false)
        reFetch()
        handleClose()
      }
    })
  }

  const pickPict = (id) => {
    setLoading(true)
    let token = JSON.parse(localStorage.getItem('token')).access_token
    setDefaultPict(token, { id }, (err, res) => {
      if (err) {
        setErrMsg(err.response.data.error.errors[0])
        setLoading(false)
      } else {
        setLoading(false)
        reFetch()
        handleClose()
      }
    })
  }

  function handelChangeImage(event) {
    let image = event.target.files[0];
    let form = new FormData();
    form.append('image', image);
    setImage(form)
  }

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Select Profile Picture</Modal.Title>
      </Modal.Header>
      <Modal.Body className="upload-pict-comp">
        <div className="galery">
          {userData.user_pictures.length > 0 &&
            userData.user_pictures.map((pict, i) => {
              return <img alt="profile-pict" onClick={() => pickPict(pict.id)} key={i} src={pict.picture.url} />
            })
          }
        </div>
        <Form>
          <Form.Group>
            <Form.Label >Uploads new picture</Form.Label>
            <Form.File onChange={(e) => handelChangeImage(e)} />
          </Form.Group>
          {loading ?
            <Spinner animation="border" role="status" /> :
            <Fragment>
              <Button variant="dark" onClick={() => setPicture()}>Upload Profile Picture</Button><br />
            </Fragment>
          }
          {errMsg ? <span style={{ color: 'red' }}>{errMsg}</span> : null}
        </Form>
      </Modal.Body>
    </Modal>
  )
}