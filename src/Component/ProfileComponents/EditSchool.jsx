import React, { useState, Fragment } from 'react'
import './style.scss'
import {Modal, Button, Form, Spinner} from 'react-bootstrap'
import {editCareer} from '../../Services'

export const EditSchoolComponent = ({show, handleClose, userData, reFetch}) => {
  const [position, setPosition] = useState(!userData.career ? "" : !userData.career.position ? "" : userData.career.position),
        [starting_from, setStarting] = useState(!userData.career ? "" : !userData.career.starting_from ? "" : userData.career.starting_from),
        [ending_in, setEnding] = useState(!userData.career ? "" : !userData.career.ending_in ? "" : userData.career.ending_in),
        [loading, setLoading] = useState(false),
        [errMsg, setErrMsg] = useState(''),
        [company_name, setCompany] = useState(!userData.career ? "" : !userData.career.company_name ? "" : userData.career.company_name);

  const changeCareer = () => {
    let payload = {
      position,
      company_name,
      starting_from,
      ending_in
    }
    setErrMsg('')
    setLoading(true)
    let token = JSON.parse(localStorage.getItem('token')).access_token
    if(position && company_name && starting_from && ending_in) {
      editCareer(token, payload, (err, res) => {
        if(err) {
          console.log(err)
          setErrMsg(err.response.data.error.errors[0])
          setLoading(false)
        } else {
          console.log(res)
          setLoading(false)
          reFetch()
          handleClose()
        }
      })
    }
  }

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Career History</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Position</Form.Label>
            <Form.Control type="text" placeholder="Enter position" value={position} onChange={({target: {value}}) => setPosition(value)}/>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Company Name</Form.Label>
            <Form.Control type="text" placeholder="Enter company name" value={company_name} onChange={({target: {value}}) => setCompany(value)}/>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Starting from</Form.Label>
            <Form.Control type="date" placeholder="Enter starting from" value={starting_from} onChange={({target: {value}}) => setStarting(value)}/>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Ending in</Form.Label>
            <Form.Control type="date" placeholder="Enter ending in" value={ending_in} onChange={({target: {value}}) => setEnding(value)}/>
          </Form.Group>

          {loading ? 
            <Spinner animation="border" role="status"/> :
            <Fragment>
              <Button variant="primary" onClick={() => changeCareer()}>Edit Career</Button><br/>
            </Fragment>
          }
          {errMsg ? <span style={{color:'red'}}>{errMsg}</span> : null}
        </Form>
      </Modal.Body>
    </Modal>
  )
}