import React, { useState, Fragment } from 'react'
import './style.scss'
import {Modal, Button, Form, Spinner} from 'react-bootstrap'
import {editEducation} from '../../Services'

export const EditEducationComponent = ({show, handleClose, userData, reFetch}) => {
  const [school_name, setName] = useState(!userData.education ? "" : !userData.education.school_name ? "" : userData.education.school_name),
        [loading, setLoading] = useState(false),
        [errMsg, setErrMsg] = useState(''),
        [graduation_time, setGradiation] = useState(!userData.education ? "" : !userData.education.graduation_time ? "" : userData.education.graduation_time);

  const changeEducation = () => {
    let payload = {
      school_name,
      graduation_time
    }
    setLoading(true)
    setErrMsg('')
    let token = JSON.parse(localStorage.getItem('token')).access_token
    if(school_name && graduation_time) {
      editEducation(token, payload, (err, res) => {
        if(err) {
          console.log(err)
          setErrMsg(err.response.data.error.errors[0])
          setLoading(false)
        } else {
          console.log(res)
          setLoading(false)
          reFetch()
          handleClose()
        }
      })
    }
  }

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Education History</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>School Name</Form.Label>
            <Form.Control type="text" placeholder="Enter school name" value={school_name} onChange={({target: {value}}) => setName(value)}/>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Graduation time</Form.Label>
            <Form.Control type="date" placeholder="Enter graduation time" value={graduation_time} onChange={({target: {value}}) => setGradiation(value)}/>
          </Form.Group>
          {loading ? 
            <Spinner animation="border" role="status"/> :
            <Fragment>
              <Button variant="primary" onClick={() => changeEducation()}> Edit</Button><br/>
            </Fragment>
          }
          {errMsg ? <span style={{color:'red'}}>{errMsg}</span> : null}
        </Form>
      </Modal.Body>
    </Modal>
  )
}