import {BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {RegisterContainer, LoginContainer, VerifyContainer, ProfilePage, MessageContainer} from './Container'

import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={RegisterContainer}><RegisterContainer/></Route>
          <Route exact path="/login" component={LoginContainer}><LoginContainer/></Route>
          <Route exact path="/verify/:phone" component={VerifyContainer}><VerifyContainer/></Route>
          <Route exact path="/profile" component={ProfilePage}><ProfilePage/></Route>
          <Route exact path="/message" component={MessageContainer}><MessageContainer/></Route>
      </Switch>
    </Router>
    </div>
  );
}

export default App;
