import axios from 'axios'

const url = "https://h8-cors-anywhere.herokuapp.com/http://pretest-qa.dcidev.id"

function registerUser(data, cb) {
  axios.post(`${url}/api/v1/register`, null,
    {
      params: data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        "Access-Control-Allow-Origin": "*",
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function registerOTP(data, cb) {
  axios.post(`${url}/api/v1/register/otp/request`, null,
    {
      params: data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        "Access-Control-Allow-Origin": "*",
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function matchOTP(data, cb) {
  axios.post(`${url}/api/v1/register/otp/match`, null,
    {
      params: data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        "Access-Control-Allow-Origin": "*",
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function loginUser(data, cb) {
  axios.post(`${url}/api/v1/oauth/sign_in`, null,
    {
      params: data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        "Access-Control-Allow-Origin": "*",
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function getProfile(token, cb) {
  axios.get(`${url}/api/v1/profile/me`,
    {
      headers: {
        "Authorization": token
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function getMessages(token, user_id, cb) {
  axios.get(`${url}/api/v1/message/${user_id}`,
    {
      headers: {
        "Authorization": token
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function sendMessage(token, data, cb) {
  axios.post(`${url}/api/v1/message/send`, data,
    {
      headers: {
        "Authorization": token,
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function editProfile(token, data, cb) {
  axios.post(`${url}/api/v1/profile`, data,
    {
      headers: {
        "Authorization": token
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function editEducation(token, data, cb) {
  axios.post(`${url}/api/v1/profile/education`, data,
    {
      headers: {
        "Authorization": token
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function editCareer(token, data, cb) {
  axios.post(`${url}/api/v1/profile/career`, data,
    {
      headers: {
        "Authorization": token
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function UploadCover(token, data, cb) {
  console.log(data);
  axios.post(`${url}/api/v1/uploads/cover`, data,
    {
      headers: {
        "Authorization": token,
        'Content-Type': 'multipart/form-data'
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function UploadProfilePict(token, data, cb) {
  axios.post(`${url}/api/v1/uploads/profile`, data,
    {
      headers: {
        "Authorization": token
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function setDefaultPict(token, data, cb) {
  axios.post(`${url}/api/v1/uploads/profile/default`, data,
    {
      headers: {
        "Authorization": token
      }
    })
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}

function revokeToken(data, cb) {
  axios.post(`${url}/api/v1/oauth/revoke`, data)
    .then(res => {
      cb(null, res)
    })
    .catch(err => {
      cb(err)
    })
}




export {
  registerUser,
  loginUser,
  registerOTP,
  matchOTP,
  getProfile,
  getMessages,
  sendMessage,
  editProfile,
  editEducation,
  editCareer,
  UploadCover,
  UploadProfilePict,
  setDefaultPict,
  revokeToken
}