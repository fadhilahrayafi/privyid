import React, {useState, useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import {Card, Container, Button, Form, Spinner} from 'react-bootstrap'
import {registerUser} from '../../Services'
import './style.scss'

export const RegisterContainer = () => {
  const [phone, setPhone] = useState(''),
        [password, setPassword] = useState(''),
        [country, setCountry] = useState(''),
        {push} = useHistory(),
        [errRes, setErrRes] = useState(''),
        [errPhone, setErrPhone] = useState(false),
        [errPass, setErrPass] = useState(false),
        [loading, setLoading] = useState(false),
        [latlong, setLatlong] = useState(''),
        [errCountry, setErrCountry] = useState(false);

  const checkDevice = () => {
    if(['iPad Simulator','iPhone Simulator','iPod Simulator','iPad','iPhone','iPod'].includes(navigator.platform)) return 0
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return 2;
    }
    if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua
      )
    ) {
      return 1;
    }
    return 2;
  }

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(getPosition);
    }
    function getPosition(position) {
      setLatlong(`${position.coords.latitude} ${position.coords.longitude}`)
      console.log(position.coords.latitude, position.coords.longitude);
    }
  }, [])
  
  const register = (e) => {
    setErrPhone(false)
    setErrPass(false)
    setErrCountry(false)
    setErrRes('')
    if(!phone) setErrPhone(true)
    if(!password) setErrPass(true)
    if(!country) setErrCountry(true)
    let payload = {
      phone,
      password,
      country,
      device_type: checkDevice(),
      latlong,
      device_token:"random12345"
    }
    console.log(payload)
    e.preventDefault()
    setLoading(true)

    if(phone && password && country) {
      registerUser(payload, (err, res) => {
        if(err) {
          console.log(err.response, "<<ER")
          setErrRes('Failed to register, please check your input')
          setLoading(false)
        }
        else {
          console.log(res, "<<<res")
          if(res.data.data.user) {
            // localStorage.setItem("user", JSON.stringify(res.data.data.user))
            push(`/verify/${res.data.data.user.phone}`)
          }
          setLoading(false)
        }
      })
    } else {
      setErrRes('Please complete the data to register')
      setLoading(false)
    }
  }

  return (
    <Container className="container-register">
      <Card className="body-register">
        <Card.Body>
          <Card.Title>Register</Card.Title>
          <span onClick={() => push('/login')} className="link-to">Click here to Login</span>
          <Form style={{textAlign:'left'}}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Phone number</Form.Label>
              <Form.Control type="text" style={{border:errPhone ? 'red 1px solid': ''}} placeholder="Enter phone number" onChange={({target: {value}}) => setPhone(value)}/>
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password"  style={{border:errPass ? 'red 1px solid': ''}} placeholder="Enter password" onChange={({target: {value}}) => setPassword(value)}/>
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Country</Form.Label>
              <Form.Control type="text" style={{border:errCountry ? 'red 1px solid': ''}}  placeholder="Enter your country" onChange={({target: {value}}) => setCountry(value)}/>
            </Form.Group>
          </Form>
          {loading ? 
            <Spinner animation="border" role="status"/> :
            <Button variant="dark" onClick={(e) => register(e)} style={{marginBottom:20}}>Register</Button>
          }
          <br/>
          {errRes && <span className="err-message">{errRes}</span>}
        </Card.Body>
      </Card>
    </Container>
  )
}