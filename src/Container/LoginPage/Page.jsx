import React, {useEffect, useState} from 'react'
import {useHistory} from 'react-router-dom'
import {Card, Container, Button, Form, Spinner} from 'react-bootstrap'
import {loginUser} from '../../Services'
import './style.scss'

export const LoginContainer = () => {
  const [phone, setPhone] = useState(''),
        {push} = useHistory(),
        [errPhone, setErrPhone] = useState(false),
        [errPass, setErrPass] = useState(false),
        [errRes, setErrRes] = useState(''),
        [loading, setLoading] = useState(''),
        [latlong, setLatlong] = useState(''),
        [password, setPassword] = useState('');

  const checkDevice = () => {
    if(['iPad Simulator','iPhone Simulator','iPod Simulator','iPad','iPhone','iPod'].includes(navigator.platform)) return 0
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return 2;
    }
    if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua
      )
    ) {
      return 1;
    }
    return 2;
  }

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(getPosition);
    }
    function getPosition(position) {
      setLatlong(`${position.coords.latitude} ${position.coords.longitude}`)
      console.log(position.coords.latitude, position.coords.longitude);
    }
  }, [])
  
  const login = () => {
    setErrPhone(false)
    setErrPass(false)
    setErrRes('')
    if(!phone) setErrPhone(true)
    if(!password) setErrPass(true)
    let payload = {
      phone,
      password,
      device_type: checkDevice(),
      latlong,
      device_token:"random12345"
    }
    console.log(payload)
    setLoading(true)
    if(phone && password) {
      loginUser(payload, (err, res) => {
        if(err) {
          console.log(err, "<<ER")
          setLoading(false)
          setErrRes('Failed to Login, please make sure to put right data')
        }
        else {
          console.log(res, "<<<res")
          setLoading(false)
          push(`/verify/${res.data.data.user.phone}`)
        }
      })
    } else {
      setErrRes('Please complete the data to login')
      setLoading(false)
    }
  }

  return (
    <Container className="container-register">
      <Card className="body-register">
        <Card.Body>
          <Card.Title>Log in</Card.Title>
          <span onClick={() => push('/')} className="link-to">Click here to Register</span>
          <Form style={{textAlign:'left'}}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Phone number</Form.Label>
              <Form.Control type="text" style={{border:errPhone ? 'red 1px solid': ''}} placeholder="Enter phone number" onChange={({target: {value}}) => setPhone(value)}/>
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" style={{border:errPass ? 'red 1px solid': ''}} placeholder="Enter password" onChange={({target: {value}}) => setPassword(value)}/>
            </Form.Group>
          </Form>
          {loading ? 
            <Spinner animation="border" role="status"/> :
            <Button variant="dark" onClick={() => login()} style={{marginBottom:20}}>Log in</Button>
          }
          <br/>
          {errRes && <span className="err-message">{errRes}</span>}
        </Card.Body>
      </Card>
    </Container>
  )
}