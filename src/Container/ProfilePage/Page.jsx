import React, {useState, useEffect} from 'react'
import {Container, Button, Alert} from 'react-bootstrap'
import {HeaderComponent, LoadingComponent, EditUserComponent, EditSchoolComponent, EditEducationComponent, UploadCoverComp, UploadProfilePictComp} from '../../Component'
import {getProfile} from '../../Services'
import './style.scss'

export const ProfilePage = () => {
  const [userInfo, setUser] = useState({}),
        [errMsg, setErrMsg] = useState(''),
        [showEditUser, setShowEditUser] = useState(false),
        [shoeEditCompany, setShowEditCompany] = useState(false),
        [showEditEdu, setShowEditEdu] = useState(false),
        [showSetCover, setShowSetCOver] = useState(false),
        [showEditProfPict, setShowEditProfPict] = useState(false),
        [loading, setLoading] = useState(false);

  const fetchProfile = () => {
    setLoading(true)
    let token = JSON.parse(localStorage.getItem('token')) 
    getProfile(token.access_token, (err, res) => {
      if(err) {
        setErrMsg('Failed get profile')
        setLoading(false)
      } else {
        console.log(res)
        if(res.data.data.user) {
          setUser(res.data.data.user)
          localStorage.setItem('user_data', JSON.stringify(res.data.data.user))
        }
        setLoading(false)
      }
    })
  }

  useEffect(() => {
    fetchProfile()
  }, [])

  return (
    <Container className="container-profile">
      <HeaderComponent name={userInfo.name ? userInfo.name : ""} selected="profile"/>
      {errMsg && <Alert  variant={'danger'}>{errMsg}</Alert>}
      {showEditUser && <EditUserComponent reFetch={fetchProfile} userData={userInfo} show={showEditUser} handleClose={() => setShowEditUser(false)}/>}
      {shoeEditCompany && <EditSchoolComponent reFetch={fetchProfile} userData={userInfo} show={shoeEditCompany} handleClose={() => setShowEditCompany(false)}/>}
      {showEditEdu && <EditEducationComponent reFetch={fetchProfile} userData={userInfo} show={showEditEdu} handleClose={() => setShowEditEdu(false)}/>}
      {showSetCover && <UploadCoverComp reFetch={fetchProfile} userData={userInfo} show={showSetCover} handleClose={() => setShowSetCOver(false)}/>}
      {showEditProfPict && <UploadProfilePictComp reFetch={fetchProfile} userData={userInfo} show={showEditProfPict} handleClose={() => setShowEditProfPict(false)}/>}
      <div className="cover" onClick={() => setShowSetCOver(true)} >
        <img title="change cover picture" alt="cover" src={userInfo.cover_picture ? userInfo.cover_picture.url : null}/>
      </div>
      { loading ? 
        <LoadingComponent/> :
        <div className="info-component">
          <div className="profile">
            <div className="avatar" >
              <img onClick={() => setShowEditProfPict(true)} title="change profile picture" alt="avatar" src={userInfo.user_picture ? userInfo.user_picture.picture.url : "https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-portrait-176256935.jpg"}/>
            </div>
            <div className="info">
              <div className="list">
                <label>Name</label>
                <span>{userInfo.name ? userInfo.name : "-"}</span>
              </div>
              <div className="list">
                <label>Age</label>
                <span>{userInfo.age ? userInfo.age : "-"}</span>
              </div>
              <div className="list">
                <label>Gender</label>
                <span>{userInfo.gender ? userInfo.gender : "-"}</span>
              </div>
              <div className="list">
                <label>Birthday</label>
                <span>{userInfo.birthday ? userInfo.birthday : "-"}</span>
              </div>
              <div className="list">
                <label>Zodiac</label>
                <span>{userInfo.zodiac ? userInfo.zodiac : "-"}</span>
              </div>
              <div className="list">
                <label>Bio</label>
                <span>{userInfo.bio ? userInfo.bio : "-"}</span>
              </div>
              <div className="list">
                <label>Hometown</label>
                <span>{userInfo.hometown ? userInfo.hometown : "-"}</span>
              </div>
            </div>
            <Button variant="outline-light" onClick={() => setShowEditUser(true)}>Edit Info</Button>
          </div>
          <div className="history">
            <div className="list-comp">
              <div className="list">
                <label className="title">Career History</label>
                <div className="detail">
                  <label>Company name</label>
                  <span>{!userInfo.career ? "-" : !userInfo.career.company_name ? "-" : userInfo.career.company_name}</span>
                </div>
                <div className="detail">
                  <label>Start from</label>
                  <span>{!userInfo.career ? "-" : !userInfo.career.starting_from ? "-" : userInfo.career.starting_from}</span>
                </div>
                <div className="detail">
                  <label>Ending</label>
                  <span>{!userInfo.career ? "-" : !userInfo.career.ending_in ? "-" : userInfo.career.ending_in}</span>
                </div>
              </div>
              <Button style={{marginTop:10}} variant="outline-dark" onClick={() => setShowEditCompany(true)}>Edit Career</Button>
            </div>
            <div className="list-comp">
              <div className="list">
                <label className="title">Education History</label>
                <div className="detail">
                  <label>School name</label>
                  <span>{!userInfo.education ? "-" : !userInfo.education.school_name ? "-" : userInfo.education.school_name}</span>
                </div>
                <div className="detail">
                  <label>Graduation time</label>
                  <span>{!userInfo.education ? "-" : !userInfo.education.graduation_time ? "-" : userInfo.education.graduation_time}</span>
                </div>
              </div>
              <Button style={{marginTop:10}} variant="outline-dark" onClick={() => setShowEditEdu(true)}>Edit Education</Button>
            </div>
          </div>
        </div>
      }
    </Container>
  )
}