import React, {useState, useEffect, Fragment} from 'react'
import {useHistory, useParams} from 'react-router-dom'
import {Card, Container, Button, Spinner} from 'react-bootstrap'
import './style.scss'
import PinInput from 'react-pin-input'
import {registerOTP, matchOTP} from '../../Services'

export const VerifyContainer = () => {
  const {push} = useHistory(),
        {phone} = useParams(),
        [id, setId] = useState(''),
        [loading, setLoading] = useState(false),
        [loadVerivy, setLoadVerify] = useState(false),
        [pin, setPin] = useState(''),
        [errMsg, setErrMsg] = useState('');
  
  const verify = () => {
    let payload = {
      user_id : id,
      otp_code : pin
    }
    setLoadVerify(true)
    matchOTP(payload, (err, res) => {
      if(err) {
        setLoadVerify(false)
        setErrMsg('Make sure you put the correct veryfication code')
      } else {
        console.log(res)
        if(res.data.data.user) {
          localStorage.setItem('token', JSON.stringify(res.data.data.user))
          localStorage.setItem('user_id', id)
          push('/profile')
        }
        setLoadVerify(false)
      }
    })
  }

  useEffect(() => {
    setLoading(true)
    registerOTP({phone}, (err, res) => {
      if(err) {
        setErrMsg('Failed to send OTP to your phone number')
        setLoading(false)
      } else {
        if(res.data.data.user) {
          setId(res.data.data.user.id)
        }
        setLoading(false)
      }
    })
    //eslint-disable-next-line
  },[])

  return (
    <Container className="container-register">
      <Card className="body-register">
        <Card.Body>
          <Card.Title>Verify your identify</Card.Title>
          {loading ? 
          <Spinner animation="border" role="status"/> :
          <Fragment>
            <Card.Subtitle>Your OTP code is already send to your phone number, verify the code by fill the PIN input</Card.Subtitle>
            <PinInput 
              length={4} 
              initialValue=""
              secret 
              onChange={(value, index) => setPin(value)} 
              type="numeric" 
              inputMode="number"
              style={{padding: '10px'}}  
              inputStyle={{borderColor: 'black'}}
              inputFocusStyle={{borderColor: 'blue'}}
              onComplete={(value, index) => {}}
              autoSelect={true}
            />
          {loadVerivy ? 
            <Spinner animation="border" role="status"/> :
            <Button variant="dark" onClick={() => verify()} style={{marginBottom:20}}>Verify</Button>
          }
          <br/>
          {errMsg && <span className="err-message">{errMsg}</span>}
          </Fragment>
          
        }
        </Card.Body>
      </Card>
    </Container>
  )
}