import React, {useState, useEffect, Fragment} from 'react'
import {Container, Button, } from 'react-bootstrap'
import {HeaderComponent, LoadingComponent} from '../../Component'
import {getMessages, sendMessage} from '../../Services'
import './style.scss'

export const MessageContainer = () => {
  const [chats, setChats] = useState([]),
        [userInfo, setUserInfo] = useState({}),
        [loading, setLoading] = useState(false),
        [token, setToken] = useState(''),
        [user_id, setUserId] = useState(''),
        [message, setMessage] = useState(''),
        [errMsg, setErrMsg] = useState(false);

  const fetchNewMsg = (tokens, id) => {
    getMessages(tokens, id, (err, res) => {
      if(err) {
        if(err.response.data.error.errors)setErrMsg(err.response.data.error.errors)
        setLoading(false)
      } else {
        if(res.data.data.chat) setChats(res.data.data.chat)
        setMessage('')
        setLoading(false)
      }
    })
  }

  const sendNewMessage = () => {
    let payload = {
      user_id,
      message
    }
    setLoading(true)
    if(message) {
      sendMessage(token, payload, (err, res) => {
        if(err) {
          if(err.response.data.error.errors)setErrMsg(err.response.data.error.errors[0])
          setLoading(false)
        } else {
          fetchNewMsg(token, user_id)
        }
      })
    }
  }

  useEffect(() => {
    let users_id = (localStorage.getItem('user_id'))
    let user = JSON.parse(localStorage.getItem('user_data')) 
    let tokens = JSON.parse(localStorage.getItem('token')).access_token 
    setToken(tokens)
    setUserId(users_id)
    setUserInfo(user)
    setLoading(true)
    fetchNewMsg(tokens, users_id)

  }, [])

  return (
    <Container >
      <HeaderComponent selected={'msg'} name={userInfo.name}/>
        <Fragment>
          <div className="message-comp">
            {chats.length === 0 && loading ?
            <LoadingComponent/> : 
              chats.map((chat, i) => {
                return (
                  <div key={i} className={`chat ${chat.type === 'sender' ? ' send' : ''}`}>
                    <span className="time">{chat.created_at ? chat.created_at : ''}</span>
                    <span className="msg">{chat.message ? chat.message : ""}</span>
                  </div>
                )
              })
          }
          </div>
          <div className="input-msg">
            <input value={message} onChange={({target: {value}}) => setMessage(value)} type="text" max={250} placeholder="type here"/>
            <Button variant={'dark'} onClick={() => sendNewMessage()}>{loading ? 'Sending...': 'Send message'} </Button><br/>
            {errMsg && <span style={{color:'red'}}>{errMsg}</span>}
          </div>
        </Fragment>
    </Container>
  )
}